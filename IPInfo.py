import requests
import json
from APIKeys import IPInfo as APIKey

def search(ip):
        response = requests.request(method='GET', url=f'https://ipinfo.io/{ip}?token={APIKey}')

        formattedDict = json.loads(response.text)
        print(f"IP: {formattedDict['ip']}\r\nLocation: {formattedDict['region']}, {formattedDict['country']}\r\nHost/ISP: {formattedDict['org']}")
