from APIKeys import AbuseIPDB as APIKey
import requests
import json
from datetime import datetime, timezone


def report(ip, comment):
	#Defining the URL
	url = 'https://api.abuseipdb.com/api/v2/report'
	#Params to pass in json format
	params = {
		'ip': ip,
		'categories':'15', #Allow these to be editable, set to 'Hacking' for now (Required for some reason)
		'comment': comment, #Get user comment for report
		'timestamp': datetime.now(timezone.utc).isoformat().replace("+00:00", "Z") #Get current datetime and convert to usable format
	}

	headers = {
		'Accept': 'application/json',
		'Key': APIKey #Get API Key
	}

	#Send request and display formatted response
	response = requests.request(method='POST', url=url, headers=headers, params=params)
	try:
		formattedDict = json.loads(response.text)['data']
		print(f"IP: {formattedDict['ipAddress']}\r\nAbuse confidence score: {formattedDict['abuseConfidenceScore']}")
	except:
		print("Error :(")
		# errorDict = json.loads(response.text)['errors']
		# print(f"Error: {errorDict['detail']}")
def search(ip):
	# Defining the api-endpoint
	url = 'https://api.abuseipdb.com/api/v2/check'

	#Setting search parameters
	params = {
	'ipAddress': ip,
	}

	#Set the headers
	headers = {
	'Accept': 'application/json',
	'Key': APIKey
	}

	#Send the request
	response = requests.request(method='GET', url=url, headers=headers, params=params)

	# Formatted output
	formattedDict = json.loads(response.text)['data']
	print(f"IP: {formattedDict['ipAddress']}\r\nPublic: {formattedDict['isPublic']}\r\nConfidence Score: {formattedDict['abuseConfidenceScore']}\r\nCountry Code: {formattedDict['countryCode']}\r\nISP: {formattedDict['isp']}\r\nDomain: {formattedDict['domain']}\r\nHostname(s): {formattedDict['hostnames']}\r\nTotal Reports: {formattedDict['totalReports']}\r\nLast reported: {formattedDict['lastReportedAt']}")
