#!/usr/bin/python3
import argparse
import json
import APIKeys
import requests


import VirusTotal
import AbuseIPDB
import IPInfo

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("ip", nargs="+", help="IP to be report/lookup")
	parser.add_argument("-V", "--vote", help="Vote on the IP (requires -vt or -ipdb)", action="count")
	parser.add_argument("-R", "--report", help="Report the IP (requires -vt or -ipdb)", action="count")
	parser.add_argument("-S", "--search", help="Search for the IP (requires -vt or -ipdb)", action="count")
	parser.add_argument("-vt", "--virustotal", help="Use VirusTotal to report/Check/Search", action="count")
	parser.add_argument("-ipdb", "--abuseipdb", help="Use AbuseIPDB to report/Check/Search", action="count")
	parser.add_argument("-ipi", "--ipinfo", help="Use ipinfo.io to report/Check/Search", action="count")
	#parser.add_argument("-vts", "--virustotal_search", help="Search the IP on VirusTotal", action="count")
	args = parser.parse_args()

	if args.report:
		global comment
		comment = input("Enter comment: ")

	for i in args.ip:
		if args.vote:
			if args.virustotal:
				VirusTotal.vtvote(i)
		if args.report:
			if args.virustotal:
				VirusTotal.vtreport(i, comment)
			if args.abuseipdb:
				AbuseIPDB.report(i, comment)
		if args.search:
			if args.virustotal:
				VirusTotal.vtsearch(i)
			if args.abuseipdb:
				AbuseIPDB.search(i)
			if args.ipinfo:
				IPInfo.search(i)
