from APIKeys import VirusTotal as APIKey
import requests
import json

def vtvote(ip):
	#Defining the API Endpoint URL
	url = f"https://www.virustotal.com/api/v3/ip_addresses/{ip}/votes"

	#Payload to pass in json format
	payload = { "data": {
	        "type": "vote",
	        "attributes": { "verdict": "malicious" } #Assuming all uses of reporting this will be malicious. To change
	    } }
	#Headers to pass in json format
	headers = {
	    "accept": "application/json",
	    "x-apikey": APIKey, #Get API Key
	    "content-type": "application/json"
	}

	#Send the push request and return the response
	response = requests.post(url, json=payload, headers=headers)
	if response.status_code == 200:
		formattedDict = json.loads(response.text)['data']
		print(f'IP: {ip}\r\nLink to report: {formattedDict['links']['self']}\r\nVote value: {formattedDict['attributes']['value']}\r\nVote verdict: {formattedDict['attributes']['verdict']}\r\n')
	elif response.status_code == 409:
		print(f"IP {ip} has already been reported.")
	else:
		print(f"Error: \r\nStatus Code: {response.status_code}\r\nError Message:{response.text}")

def vtreport(ip, comment):
	#Defining the API Endpoint URL
	url = f"https://www.virustotal.com/api/v3/ip_addresses/{ip}/comments"

	#Payload to pass in json format
	payload = { "data": {
	        "type": "comment",
	        "attributes": { "text": comment }
	    } }
	#Headers to pass in json format
	headers = {
	    "accept": "application/json",
	    "x-apikey": APIKey, #Get API Key
	    "content-type": "application/json"
	}

	#Send the push request and return the response
	response = requests.post(url, json=payload, headers=headers)
	if response.status_code == 200:
		formattedDict = json.loads(response.text)['data']
		print(f"IP: {ip}\r\nLink to report: {formattedDict['links']['self']}\r\nPositive Votes: {formattedDict['attributes']['votes']['positive']}\r\nNegative Votes: {formattedDict['attributes']['votes']['negative']}")
	elif response.status_code == 409:
		print(f"IP {ip} has already been reported.")
	else:
		print(f"Error: \r\nStatus Code: {response.status_code}\r\nError Message:{response.text}")

def vtsearch(ip):
	#Defining the API Endpoint
	url = f"https://www.virustotal.com/api/v3/ip_addresses/{ip}"
	headers = {
		"accept": "application/json",
		"x-apikey": APIKey
			}
	#Send the get request and print the result
	response = requests.get(url, headers=headers)
	formattedDict = json.loads(response.text)['data']
	print(f"IP: {ip}\r\nLink to report: {formattedDict['links']['self']}\r\nMalicious Detections: {formattedDict['attributes']['last_analysis_stats']['malicious']}\r\nSuspicious Detections: {formattedDict['attributes']['last_analysis_stats']['suspicious']}\r\nHarmless Detections: {formattedDict['attributes']['last_analysis_stats']['harmless']}\r\nMalicious Community votes: {formattedDict['attributes']['total_votes']['malicious']}\r\nISP: {formattedDict['attributes']['as_owner']}\r\nCountry: {formattedDict['attributes']['country']}")
